﻿using System;
using System.IO;
using System.Management;

namespace osu_Share_ex
{
    public static class Log
    {
        static string AppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        static string FolderPath = Path.Combine(AppDataPath, "osu!Share");
        static string LogPath = Path.Combine(FolderPath, "logs");

        private static StreamWriter Writer;

        private static int SystemTicksOnStart;

        //static SettingsManager settingsmanager = new SettingsManager();

        static bool Initialized = false;

        internal static void Iniatialize(int LogInt)
        {
            if (Initialized)
                return;

            if (!Directory.Exists(LogPath))
                Directory.CreateDirectory(LogPath);

            Writer = new StreamWriter(Path.Combine(LogPath, "osu!Share_" + LogInt.ToString() + ".log"), false);
            Writer.AutoFlush = true;

            Writer.WriteLine("Logger opened, start: " + osu_Share_BETA.Properties.Settings.Default.Start.ToString());

            Writer.WriteLine("--- Basic system information ---");
            Writer.WriteLine("  Cores: " + Environment.ProcessorCount);
            Writer.WriteLine("  Is64Bit: " + Environment.Is64BitOperatingSystem.ToString());
            Writer.WriteLine("  Is64BitProcess: " + Environment.Is64BitProcess.ToString());
            Writer.WriteLine("  Version: " + Environment.Version);
            Writer.WriteLine("  CurrentThreadId: " + Environment.CurrentManagedThreadId);
            SystemTicksOnStart = Environment.TickCount;
            Writer.WriteLine("  OSRuntime: " + SystemTicksOnStart);

            osu_Share_BETA.Properties.Settings.Default.LogInt++;
            Initialized = true;
        }

        internal static void Close()
        {
            Writer.WriteLine("Run: " + (Environment.TickCount - SystemTicksOnStart).ToString());
            Writer.WriteLine("Logger closed");

            Writer.Close();
            Writer.Dispose();
        }

        internal static void LogMessage(string message)
        {
            string CompiledMessage = "(Message)[" + DateTime.UtcNow + "] " + message;
            Writer.WriteLine(CompiledMessage);
        }

        #region LogError
        internal static void LogError(string message)
        {
            string CompiledMessage = "(Error)[" + DateTime.UtcNow + "] " + message;
            Writer.WriteLine(CompiledMessage);
        }

        internal static void LogError(string message, Exception exception)
        {
            bool hasInnerException = true;
            string CompiledMessage2 = null;

            string CompiledMessage = "(" + exception.GetType() + ")[" + DateTime.UtcNow + "] " +
                message + Environment.NewLine + "Error information" + Environment.NewLine +
                "Exception message: " + exception.Message + Environment.NewLine +
                "Stack trace" + Environment.NewLine +
                exception.StackTrace;

            try
            {
                CompiledMessage2 = "--- Error (" + exception.GetType() + ") Happened at [" + DateTime.UtcNow + " ---" + Environment.NewLine +
                    "   Message: " + message + Environment.NewLine +
                    "   --- Error information ---" + Environment.NewLine +
                    "   Exception message: " + exception.Message + Environment.NewLine +
                    "   Stack trace(Main exception)" + Environment.NewLine +
                    exception.StackTrace + Environment.NewLine +
                    "   --- InnerException (" + exception.InnerException.GetType() + ") ---" + Environment.NewLine +
                    "   Exception message: " + exception.InnerException.Message + Environment.NewLine +
                    "   Stack trace(InnerException)" + Environment.NewLine +
                    exception.InnerException.StackTrace;
            }
            catch { hasInnerException = false; }

            if (hasInnerException)
                if (CompiledMessage2 != null)
                    Writer.WriteLine(CompiledMessage2);
            else
                Writer.WriteLine(CompiledMessage);
        }
        #endregion
        
        internal static void LogInfo(string message)
        {
            string CompiledMessage = "(Info)[" + DateTime.UtcNow + "] " + message;
            Writer.WriteLine(CompiledMessage);
        }
    }
}
