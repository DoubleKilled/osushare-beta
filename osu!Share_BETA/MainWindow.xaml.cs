﻿using MahApps.Metro.Controls;
using osu_Share_ex;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;

namespace osu_Share_BETA
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        static string AppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        string FolderPath = System.IO.Path.Combine(AppDataPath, "osu!Share");

        internal MainWindow form;
        Read2 read2 = new Read2();
        Download download = new Download();
        Lister lister = new Lister();
        Installer installer = new Installer();
        Lister2 lister2 = new Lister2();

        Dictionary<CheckBox, BeatmapInfo2> ListItems = new Dictionary<CheckBox, BeatmapInfo2>();
        bool SelectState = false;
        Queue<DownloadStruct> dlQueue = new Queue<DownloadStruct>();
        List<DownloadMessage> currentDownloads = new List<DownloadMessage>();
        Dictionary<CheckBox, dlPanelStruct> downloadPanel = new Dictionary<CheckBox, dlPanelStruct>();

        string pPath;
        decimal Quemax;
        decimal DownloadsDone;
        bool downloading;

        public MainWindow()
        {
            InitializeComponent();

            string[] args = Environment.GetCommandLineArgs();
            foreach (string arg in args)
            {
                if (arg == "/dev")
                    Main.Dev = true;
            }

            form = this;
            Main.MainForm = form;

            DownloadLabel.Visibility = Visibility.Hidden;
            Log.Iniatialize(Properties.Settings.Default.LogInt);
            if (Main.Dev == true)
            {
                Log.LogInfo("DEV MODE ON (prepare your butts for log spam)");
                SettingsTab.IsEnabled = true;
            }


            HeaderProgressBar.Visibility = Visibility.Hidden;

            AppDomain.CurrentDomain.UnhandledException += (sender, e) => GeneralErrorHandler(e);
            Properties.Settings.Default.Start++;

            if (Properties.Settings.Default.CallUpgrade)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.CallUpgrade = false;
            }
        }

        private void GeneralErrorHandler(UnhandledExceptionEventArgs e)
        {
            // Handles all unhandled error
            Log.LogError("Unexpected error", (Exception)e.ExceptionObject);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(Properties.Settings.Default.Path))
            {
                pPath = Properties.Settings.Default.Path;
                ListerPathbox.Text = Properties.Settings.Default.Path;
            }

            if (File.Exists(System.IO.Path.Combine(FolderPath, "np.xml")))
            {
                string p = System.IO.Path.Combine(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "osu!Share"), "np.xml");
                SecondaryTextBox.Text = p;
            }
            Task<bool> bcCheck = Task<bool>.Factory.StartNew(() =>
                {
                    try
                    {
                        HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://bloodcat.com/osu/");
                        HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                        resp.Close();

                        if (resp.StatusCode == HttpStatusCode.OK)
                            return true;
                        else
                            return false;
                    }
                    catch
                    {
                        return false;
                    }
                });
            if (!bcCheck.Result)
            {
                MessageBox.Show("I didn't hear from bloodcat! Disabling downloader");
                DownloaderTab.IsEnabled = false;
                if (MainSelector.SelectedIndex == 0)
                    MainSelector.SelectedIndex = 1;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            List<BeatmapInfo2> data = null;
            ListItems.Clear();
            ProcessedListBox.Items.Clear();

            int IND = 0;

            if (!(bool)CompareCheckBox.IsChecked)
            {
                if (MainTextBox.Text == null)
                    data = read2.GetList();
                else
                    data = read2.GetList(path: MainTextBox.Text);

                if (ProcessedListBox.Items.Count > 0)
                    return;

                if (data == null)
                    return;

                foreach (BeatmapInfo2 b in data)
                {
                    CheckBox c = new CheckBox();
                    c.Content = b.MapName + " - " + b.ID;

                    ProcessedListBox.Items.Add(c);
                    ListItems.Add(c, b);

                    IND++;
                }
            }
            else
            {
                if (MainTextBox == null)
                    return;

                if (MainTextBox.Text == null && SecondaryTextBox.Text == null)
                    data = read2.Compare();
                else if (MainTextBox.Text != null && SecondaryTextBox.Text != null)
                    data = read2.Compare(mainpath: MainTextBox.Text, secondarypath: SecondaryTextBox.Text);
                else if (MainTextBox.Text != null && SecondaryTextBox.Text == null)
                    data = read2.Compare(mainpath: MainTextBox.Text);
                else if (MainTextBox.Text == null && SecondaryTextBox.Text != null)
                    data = read2.Compare(secondarypath: SecondaryTextBox.Text);

                if (ProcessedListBox.Items.Count > 0)
                    return;

                if (data != null)
                {
                    foreach(BeatmapInfo2 b in data)
                    {
                        CheckBox c = new CheckBox();
                        c.Content = b.MapName + " - " + b.ID;

                        ProcessedListBox.Items.Add(c);
                        ListItems.Add(c, b);

                        IND++;
                    }
                }
                else
                {
                    Log.LogError("Unable to retrieve data, most likely caused by invalid files");
                }
            }

            if (FilterSelector.Text != "None")
            {
                var mode = Main.ModeConver(FilterSelector.Text);

                if (!string.IsNullOrEmpty(mode))
                {
                    foreach (var item in ListItems)
                    {
                        if (item.Value.Modes.Contains(mode))
                            item.Key.IsChecked = true;
                    }
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ListItems.Clear();
            ProcessedListBox.Items.Clear();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (!SelectState)
            {
                foreach (CheckBox c in ProcessedListBox.Items)
                {
                    c.IsChecked = true;
                }
                SelectState = true;
            }
            else
            {
                foreach (CheckBox c in ProcessedListBox.Items)
                {
                    c.IsChecked = false;
                }
                SelectState = false;
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            if (MainTextBox.Text.Contains("osu.ppy.sh/b/") || MainTextBox.Text.Contains("bloodcat.com/osu/s/"))
            {
                string ParsedText = MainTextBox.Text;

                if (MainTextBox.Text.Contains("osu.ppy.sh/b/"))
                {
                    ParsedText = ParsedText.Replace("https://osu.ppy.sh/b/", "");
                    ParsedText = ParsedText.Replace("osu.ppy.sh/b/", "");
                }
                else if (MainTextBox.Text.Contains("bloodcat.com/osu/s/"))
                {
                    ParsedText = ParsedText.Replace("http://bloodcat.com/osu/s/", "");
                    ParsedText = ParsedText.Replace("bloodcat.com/osu/s/", "");
                }
                else
                    // Just in case
                    return;

                // TODO: Finish code
                // Current problems:
                //  - No way of knowing single dl from bundled one -> another method
                //  - UI Bugs -> (again) another method

                return;
            }

            Quemax = 0;
            DownloadsDone = 0;
            dlQueue.Clear();
            foreach (CheckBox c in ProcessedListBox.Items)
            {
                if (c.IsChecked == true)
                {
                    DownloadStruct d = new DownloadStruct();
                    d.beatmap = ListItems[c];
                    d.ind = c;
                    dlQueue.Enqueue(d);
                }
            }
            Quemax = dlQueue.Count;
            if (Quemax == 0)
                return;

            HeaderProgressBar.Width = 0;

            HeaderDefaultColor.Visibility = Visibility.Hidden;
            HeaderProgressBar.Visibility = Visibility.Visible;

            DownloadLabel.Content = "Downloading.. " + DownloadsDone.ToString() + "/" + Quemax.ToString();
            DownloadLabel.Visibility = Visibility.Visible;

            ProcessedListBox.IsEnabled = false;
            DLProcess.IsEnabled = false;
            DLClear.IsEnabled = false;
            DLSelect.IsEnabled = false;
            DLDownload.IsEnabled = false;
            QueNext();
        }

        void QueNext()
        {
            if (dlQueue.Count == 0)
            {
                HeaderProgressBar.Width = 792;
                HeaderProgressBar.Visibility = Visibility.Hidden;
                HeaderDefaultColor.Visibility = Visibility.Visible;

                DownloadLabel.Visibility = Visibility.Hidden;

                ProcessedListBox.IsEnabled = true;
                DLProcess.IsEnabled = true;
                DLClear.IsEnabled = true;
                DLSelect.IsEnabled = true;
                DLDownload.IsEnabled = true;
                downloading = false;
                return;
            }
            else
            {
                downloading = true;
                int width = (int)Math.Round(792m * (DownloadsDone / Quemax), MidpointRounding.ToEven);
                HeaderProgressBar.Width = width;
                HeaderProgressBar.Visibility = Visibility.Visible;
                UpdateLayout();
            }

            if (downloadPanel.Count < (2 - downloadPanel.Count))
            {
                for (int i = 0; i < 2; i++)
                {
                    try
                    {
                        DownloadMessage M = new DownloadMessage();
                        var data = dlQueue.Dequeue();
                        M.Beatmap = data.beatmap;
                        M.ind = data.ind;
                        download.DownloadAsync(M, form);

                        currentDownloads.Add(M);
                    }
                    catch { }
                }
            }
            DownloadLabel.Content = "Downloading.. " + DownloadsDone.ToString() + "/" + Quemax.ToString();
        }

        internal struct DownloadStruct
        {
            internal BeatmapInfo2 beatmap;
            internal CheckBox ind;
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.Path != null)
                pPath = Properties.Settings.Default.Path;

            string body = lister2.CreateXML(pPath, (bool)ListerLocalnp.IsChecked);

            if ((bool)ListerPushToPastebin.IsChecked)
            {
                if (string.IsNullOrWhiteSpace(body))
                {
                    Log.LogError("Pastebin data was empty!");
                    MessageBox.Show("Something went wrong!");
                    return;
                }
                var url = Pastebin.Paste(body);
                ListerURLBox.Text = url;
            }
        }

        private void ListerPathbox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ListerPathbox.Text = dialog.SelectedPath;
                pPath = dialog.SelectedPath;

                Properties.Settings.Default.Path = dialog.SelectedPath;
            }
        }

        public void CreatePanel(DownloadMessage dlmessage)
        {
            dlPanelStruct dl = new dlPanelStruct();

            dl.dlPanel = new Grid();
            dl.dlPanel.Height = 75;
            dl.dlPanel.Width = 217;
            dl.dlPanel.Background = Brushes.WhiteSmoke;

            dl.dlPanelLabel = new Label();
            dl.dlPanelLabel.Content = dlmessage.Beatmap.MapName;
            dl.dlPanel.Children.Add(dl.dlPanelLabel);
            Canvas.SetTop(dl.dlPanelLabel, 3d);
            Canvas.SetLeft(dl.dlPanelLabel, 3d);

            dl.dlPanelProgress = new ProgressBar();
            dl.dlPanelProgress.Width = 211;
            dl.dlPanelProgress.Height = 25;
            dl.dlPanelProgress.Minimum = 0;
            dl.dlPanelProgress.Maximum = 100;
            dl.dlPanel.Children.Add(dl.dlPanelProgress);

            dlPanelHold.Children.Add(dl.dlPanel);
            Canvas.SetLeft(dl.dlPanel, 10d);
            Canvas.SetTop(dl.dlPanel, 10d + (downloadPanel.Count * 75d) + (downloadPanel.Count * 3d));

            downloadPanel.Add(dlmessage.ind, dl);
            Log.LogInfo("dlPanel created for: " + dlmessage.Beatmap.MapName);
        }

        public void UpdatePanel(DownloadMessage dlmessage, object sender, System.Net.DownloadProgressChangedEventArgs e)
        {
            if (!dlmessage.Handled)
            {
                try
                {
                    dlPanelStruct dl = downloadPanel[dlmessage.ind];
                    dl.dlPanelProgress.Value = e.ProgressPercentage;
                    UpdateLayout();
                }
                catch (Exception e2)
                {
                    Log.LogError("Failed to update panel for map " + dlmessage.Beatmap.MapName + "! Process: " + e.ProgressPercentage + "%", e2);
                }
            }
            else
            {
                Log.LogError("Skipped updating panel for map " + dlmessage.Beatmap.MapName + " because it has been handled!");
            }
        }

        public void RemovePanel(DownloadMessage dlmessage)
        {
            try
            {
                dlPanelStruct dl = downloadPanel[dlmessage.ind];
                dlPanelHold.Children.Remove(dl.dlPanel);

                downloadPanel.Remove(dlmessage.ind);
                DownloadsDone++;
                QueNext();
            }
            catch (Exception e)
            {
                Log.LogError("Failed to update panel for map" + dlmessage.Beatmap.MapName, e);
            }
        }

        public struct dlPanelStruct
        {
            public Grid dlPanel;
            public Label dlPanelLabel;
            public ProgressBar dlPanelProgress;
        }

        private void ProcessedListBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (downloading)
                e.Handled = true;
        }

        private void OverwriteSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Properties.Settings.Default.OverwriteMode = OverwriteSelector.SelectedIndex;
        }

        private void Form_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Setting.ShutdownSave();
        }
    }
}
