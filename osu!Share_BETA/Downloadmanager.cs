﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Threading;
using System.Xml;

namespace osu_Share_ex
{
    internal static class Main
    {
        internal static bool Dev = false;
        internal static osu_Share_BETA.MainWindow MainForm;

        static internal string ModeConver(string text)
        {
            switch (text.ToLower())
            {
                case "osu!":
                    return "0";
                case "taiko":
                    return "1";
                case "catch the beat":
                    return "2";
                case "osu!mania":
                    return "3";
                default:
                    return null;
            }
        }

        static internal bool CheckOsuRunning()
        {
            Process[] process = Process.GetProcessesByName("osu!");
            if (process.Length > 0)
                return true;
            else
                return false;
        }

        static internal string Base64Encode(string plaintext)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plaintext);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        static internal string Base64Decode(string encodedtext)
        {
            var bytes = System.Convert.FromBase64String(encodedtext);
            return System.Text.Encoding.UTF8.GetString(bytes);
        }
    }

    internal class Download
    {
        static string AppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        string FolderPath = Path.Combine(AppDataPath, "osu!Share");

        string Filename = null;

        internal Thread DownloadAsync(DownloadMessage downloadMessage, osu_Share_BETA.MainWindow form)
        {
            // TODO:
            //   - Threading system to BackgroundWorker or Task
            Thread thread = new Thread(() =>
            {
                Stopwatch elapsed = new Stopwatch();
                try
                {
                    form.Dispatcher.BeginInvoke(((System.Action)delegate()
                    {
                        form.CreatePanel(downloadMessage);
                    }));
                    elapsed.Start();

                    if (!downloadMessage.Beatmap.ID.IsNumeric())
                    {
                        elapsed.Stop();
                        form.Dispatcher.BeginInvoke((System.Action)delegate()
                        {
                            form.RemovePanel(downloadMessage);
                        });
                        Log.LogInfo("Removed panel for song " + downloadMessage.Beatmap.MapName + " because it had a weird ID! (Non-numeric ID) [" + elapsed.ToString() + "]");
                    }

                    HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(new Uri("http://bloodcat.com/osu/s/" + downloadMessage.Beatmap.ID));
                    HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                    resp.Close();
                    var filename = resp.GetResponseHeader("Content-Disposition").Replace("attachment; filename=", "");
                    filename = filename.Replace("\"", "");

                    WebClient client = new WebClient();

                    client.DownloadProgressChanged += (s, e) =>
                        {
                            form.Dispatcher.BeginInvoke((System.Action)delegate()
                            {
                                form.UpdatePanel(downloadMessage, s, e);
                            });
                        };
                    client.DownloadDataCompleted += (s, e) =>
                        {
                            try
                            {
                                byte[] data = e.Result;
                                if (File.Exists(Path.Combine(Path.Combine(FolderPath, "tmp"), filename)))
                                {
                                    if (osu_Share_BETA.Properties.Settings.Default.OverwriteMode == 1)
                                    {
                                        elapsed.Stop();
                                        File.Delete(Path.Combine(Path.Combine(FolderPath, "tmp"), filename));
                                        File.WriteAllBytes(Path.Combine(Path.Combine(FolderPath, "tmp"), filename), data);

                                        form.Dispatcher.BeginInvoke((System.Action)delegate()
                                        {
                                            form.RemovePanel(downloadMessage);
                                        });
                                        downloadMessage.Handled = true;
                                        Log.LogInfo("dlPanel for beatmap " + downloadMessage.Beatmap.MapName + " removed [" + elapsed.ToString() + "]");

                                        Array.Clear(data, 0, data.Length);
                                        client.Dispose();
                                        return;
                                    }
                                    else if (osu_Share_BETA.Properties.Settings.Default.OverwriteMode == 2)
                                    {
                                        elapsed.Stop();
                                        form.Dispatcher.BeginInvoke((System.Action)delegate()
                                        {
                                            form.RemovePanel(downloadMessage);
                                        });
                                        downloadMessage.Handled = true;
                                        Log.LogInfo("dlPanel for beatmap " + downloadMessage.Beatmap.MapName + " removed [" + elapsed.ToString() + "]");

                                        Array.Clear(data, 0, data.Length);
                                        client.Dispose();
                                        return;
                                    }

                                    if (System.Windows.MessageBox.Show("File " + filename + " already exists in tmp folder" + Environment.NewLine + "Do you want to overwrite it?", "", System.Windows.MessageBoxButton.YesNo) == System.Windows.MessageBoxResult.Yes)
                                    {
                                        elapsed.Stop();
                                        File.Delete(Path.Combine(Path.Combine(FolderPath, "tmp"), filename));
                                        File.WriteAllBytes(Path.Combine(Path.Combine(FolderPath, "tmp"), filename), data);

                                        form.Dispatcher.BeginInvoke((System.Action)delegate()
                                        {
                                            form.RemovePanel(downloadMessage);
                                        });
                                        Log.LogInfo("dlPanel for beatmap " + downloadMessage.Beatmap.MapName + " removed [" + elapsed.ToString() + "]");
                                        downloadMessage.Handled = true;
                                    }
                                    else
                                    {
                                        elapsed.Stop();
                                        form.Dispatcher.BeginInvoke((System.Action)delegate()
                                        {
                                            form.RemovePanel(downloadMessage);
                                        });
                                        Log.LogInfo("dlPanel for beatmap " + downloadMessage.Beatmap.MapName + " removed [" + elapsed.ToString() + "]");
                                        downloadMessage.Handled = true;

                                        Array.Clear(data, 0, data.Length);
                                        client.Dispose();
                                        return;
                                    }
                                }
                                else
                                {
                                    elapsed.Stop();
                                    form.Dispatcher.BeginInvoke((System.Action)delegate()
                                    {
                                        form.RemovePanel(downloadMessage);
                                    });
                                    Log.LogInfo("dlPanel for beatmap " + downloadMessage.Beatmap.MapName + " removed [" + elapsed.ToString() + "]");
                                    downloadMessage.Handled = true;

                                    File.WriteAllBytes(Path.Combine(Path.Combine(FolderPath, "tmp"), filename), data);
                                    Array.Clear(data, 0, data.Length);
                                    client.Dispose();
                                    return;
                                }

                                Array.Clear(data, 0, data.Length);
                                client.Dispose();
                                return;
                            }
                            catch (Exception e2)
                            {
                                elapsed.Stop();
                                form.Dispatcher.BeginInvoke((System.Action)delegate()
                                {
                                    form.RemovePanel(downloadMessage);
                                });
                                Log.LogInfo("dlPanel for beatmap " + downloadMessage.Beatmap.MapName + " removed [" + elapsed.ToString() + "]");
                                downloadMessage.Handled = true;

                                client.Dispose();
                                Log.LogError("Download aborted for beatmap " + downloadMessage.Beatmap.MapName, e2);
                                return;
                            }
                        };
                    client.DownloadDataAsync(new System.Uri("http://bloodcat.com/osu/s/" + downloadMessage.Beatmap.ID));
                    this.Filename = filename;
                }
                catch (Exception e)
                {
                    elapsed.Stop();
                    form.Dispatcher.BeginInvoke((System.Action)delegate()
                    {
                        form.RemovePanel(downloadMessage);
                    });
                    Log.LogInfo("dlPanel for beatmap " + downloadMessage.Beatmap.MapName + " removed [" + elapsed.ToString() + "]");
                    downloadMessage.Handled = true;

                    Log.LogError("Download aborted for beatmap " + downloadMessage.Beatmap.MapName, e);
                    return;
                }
            });
            thread.Name = "DownloaderThread";
            thread.Start();
            return thread;
        }

        internal enum StatusCode
        {
            Success,
            Fail,
            Error
        }
    }

        public class Installer
        {
            static string AppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string FolderPath = Path.Combine(AppDataPath, "osu!Share");

            public void InstallFiles()
            {
                Thread thread = new Thread((t) =>
                {
                    string[] Files = Directory.GetFiles(Path.Combine(FolderPath, "tmp"));
                    foreach (string file in Files)
                    {
                        string FileName = Path.GetFileNameWithoutExtension(file);
                        if (osu_Share_BETA.Properties.Settings.Default.Path == null)
                        {
                            System.Windows.MessageBox.Show("Please set your osu! songs folder location before trying to install beatmaps");
                            return;
                        }
                        else
                        {
                            if (Directory.Exists(Path.Combine(osu_Share_BETA.Properties.Settings.Default.Path, FileName)))
                            {
                                if (System.Windows.MessageBox.Show("You already have beatmap " + FileName + " installed" + Environment.NewLine + "Do you want to overwrite it?", "", System.Windows.MessageBoxButton.YesNo) == System.Windows.MessageBoxResult.Yes)
                                {
                                    Directory.Delete(Path.Combine(osu_Share_BETA.Properties.Settings.Default.Path, FileName));
                                    Directory.CreateDirectory(Path.Combine(osu_Share_BETA.Properties.Settings.Default.Path, FileName));
                                    ZipFile.ExtractToDirectory(file, Path.Combine(osu_Share_BETA.Properties.Settings.Default.Path, FileName));

                                    File.Delete(file);
                                }
                                else
                                {
                                    File.Delete(file);
                                    continue;
                                }
                            }
                            else
                            {
                                Directory.CreateDirectory(Path.Combine(osu_Share_BETA.Properties.Settings.Default.Path, FileName));
                                ZipFile.ExtractToDirectory(file, Path.Combine(osu_Share_BETA.Properties.Settings.Default.Path, FileName));
                                File.Delete(file);
                            }
                        }
                    }
                });
                thread.Name = "BeatmapInstallerThread";
                thread.Start();
            }
        }
    }
