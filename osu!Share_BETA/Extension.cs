﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace osu_Share_ex
{
    /// <summary>
    /// Extensions for System.String
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// Checks if System.String contains only numeric characters
        /// </summary>
        /// <param name="str">System.String, string to be compared</param>
        /// <returns>System.Boolean, true if the whole string is numeric, otherwise false</returns>
        public static bool IsNumeric(this string str)
        {
            float v;
            return float.TryParse(str, out v);
        }
    }
}
