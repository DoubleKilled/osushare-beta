﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Shapes;
using System.Xml;

namespace osu_Share_ex
{
    public class Lister
    {
        static string AppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        string FolderPath = System.IO.Path.Combine(AppDataPath, "osu!Share");

        // Note: Dictionary would work too
        private List<Tuple<string, string>> GetList(string path)
        {
            if (Directory.Exists(path))
            {
                List<Tuple<string, string>> Built = new List<Tuple<string, string>>();
                string[] Directories;
                Directories = Directory.GetDirectories(path);

                foreach (string sub in Directories)
                {
                    Built.Add(new Tuple<string, string>(System.IO.Path.GetFileName(sub), sub));
                }
                return Built;
            }
            else
                return null;
        }

        [Obsolete("Use Lister2 instead")]
        public bool CreateFile(string path)
        {
            List<BeatmapInfo> Data = CreateList(path);
            if (Data == null)
                return false;

            var w = new StreamWriter("np.txt", false);
            foreach (BeatmapInfo b in Data)
            {
                /*  Template
                 *  Beatmap Name || Beatmap URL
                 *      - Difficulty1 (Creator)
                 *      - Difficulty2 (Creator)
                 *      - Difficulty3 (Creator)
                 *      - Difficulty4 (Creator)
                 */

                w.WriteLine(b.MapName + " || " + b.MapURL);
                foreach (string d in b.Difficulty)
                {
                    w.WriteLine("   - " + d);
                }
                w.WriteLine("");
            }
            w.Close();
            return true;
        }

        [Obsolete("Use Lister2 instead")]
        public bool CreateXML(string path, bool local)
        {
            List<BeatmapInfo> Data = CreateList(path);
            if (Data == null)
                return false;
            XmlWriterSettings ws = new XmlWriterSettings();
            ws.Indent = true;
            
            XmlWriter w;
            if (local)
                w = XmlWriter.Create(System.IO.Path.Combine(FolderPath, "np.xml"));
            else
                w = XmlWriter.Create("xml.np", ws);

            w.WriteStartDocument();
            w.WriteComment("This file has been created using osu!Share on " + System.DateTime.UtcNow + " UTC");
            w.WriteStartElement("Root");

            foreach (BeatmapInfo b in Data)
            {
                w.WriteStartElement("Item");
                w.WriteAttributeString("Name", b.MapName);
                w.WriteElementString("ID", b.ID);
                w.WriteElementString("MapURL", b.MapURL);
                foreach (string q in b.Difficulty)
                    w.WriteElementString("Difficulty", q);
                foreach (string m in b.Mode)
                    w.WriteElementString("Mode", m);
                w.WriteEndElement();
            }

            w.WriteEndElement();
            w.WriteEndDocument();
            w.Close();

            return true;
        }

        private List<BeatmapInfo> CreateList(string path)
        {
            List<Tuple<string, string>> Data = GetList(path);
            List<BeatmapInfo> MapInfo = new List<BeatmapInfo>();
            if (Data != null)
            {
                Data.RemoveAll(item => item.Item1 == "tutorial");
                foreach (Tuple<string, string> s in Data)
                {
                    try
                    {
                        string[] parts = s.Item1.Split(new string[] { " " }, 2, StringSplitOptions.None);
                        BeatmapInfo b = new BeatmapInfo();
                        b.ID = parts[0];
                        b.MapName = parts[1];
                        b.MapURL = "https://osu.ppy.sh/s/" + parts[0];
                        var ret = getOsuData(s.Item2, parts[1]);
                        b.Difficulty = ret.Difficulties;
                        b.Mode = ret.Modes;

                        MapInfo.Add(b);
                    }
                    catch (Exception e)
                    {
                        Log.LogError("Error while creating xml", e);
                    }
                }
            }
            else
                return null;

            return MapInfo;
        }

        private DifficultyReturn getOsuData(string path, string SongName)
        {
            DifficultyReturn dReturn = new DifficultyReturn();
            List<string> Modes = new List<string>();

            string[] Files = Directory.GetFiles(path);
            List<Tuple<string, string>> Parsed = new List<Tuple<string, string>>();
            List<string> Final = new List<string>();

            foreach (string s in Files)
            {
                if (s.Contains(".osu"))
                {
                    string mode = ReadFile(s);
                    mode = mode.Replace("Mode: ", "");
                    if (!Modes.Contains(mode))
                        Modes.Add(mode);

                    var p = System.IO.Path.GetFileNameWithoutExtension(s);

                    p = p.Replace(SongName, "");
                    var sb = new StringBuilder();

                    // For getting creator
                    bool instr = false;
                    int CharInd1 = -1;
                    int CharInd2 = -1;

                    // For getting difficulty
                    bool instr2 = false;
                    int CharInt3 = -1;
                    int CharInt4 = -1;


                    string Creator = "N/A";
                    string Difficulty = "N/A";

                    for (int i = 0; i < p.Length; i++)
                    {
                        char c = p[i];
                        if (c == '(')
                        {
                            instr = true;
                            CharInd1 = i;
                        }
                        else if (c == ')')
                        {
                            CharInd2 = i;
                            if (instr)
                            {
                                if (CharInd1 != -1 && CharInd2 != -1)
                                {
                                    var length = CharInd2 - CharInd1;
                                    Creator = p.Substring(CharInd1 + 1, length - 1);
                                }
                            }
                        }
                        else if (c == '[')
                        {
                            instr2 = true;
                            CharInt3 = i;
                        }
                        else if (c == ']')
                        {
                            CharInt4 = i;
                            if (instr2)
                            {
                                if (CharInt3 != -1 && CharInt4 != -1)
                                {
                                    var length = CharInt4 - CharInt3;
                                    Difficulty = p.Substring(CharInt3 + 1, length - 1);
                                }
                            }
                        }
                    }

                    Parsed.Add(new Tuple<string, string>(Creator, Difficulty));
                }
            }
            foreach (Tuple<string, string> s in Parsed)
            {
                Final.Add(s.Item2 + " (" + s.Item1 + ")");
            }


            dReturn.Difficulties = Final;
            dReturn.Modes = Modes;

            return dReturn;
        }

        private string ReadFile(string path)
        {
            string[] file = File.ReadAllLines(path);
            foreach (string line in file)
            {
                if (line.Contains("Mode"))
                {
                    return line;
                }
            }
            return "N/A";
        }
    }

    public class BeatmapInfo
    {
        public string ID { get; set; }
        public string MapName { get; set; }
        public string MapURL { get; set; }

        public List<string> Difficulty = new List<string>();
        public List<string> Mode = new List<string>();

    }

    public class DifficultyReturn
    {
        public List<string> Difficulties = new List<string>();
        public List<string> Modes = new List<string>();
    }

    public struct DownloadMessage
    {
        public CheckBox ind;

        public int Left;
        public int Top;
        public BeatmapInfo2 Beatmap;

        public bool Handled;
    }
}
