﻿using System;
using System.IO;
using System.Xml;
using osu_Share_BETA;

namespace osu_Share_ex
{
    internal static class Setting
    {
        internal static void ShutdownSave()
        {
            Log.LogInfo("Saving settings");
            osu_Share_BETA.Properties.Settings.Default.Save();
            Log.Close();
        }
    }
}