﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace osu_Share_ex
{
    internal static class Pastebin
    {
        private static string APIKey = "725df15191c3bc51796bb42c9da6eb13";
        internal static string Paste(string body)
        {
            Task<string> pushPaste = Task<string>.Factory.StartNew(() =>
                {
                    if (string.IsNullOrEmpty(body.Trim()))
                        throw new NullReferenceException("Body cannot be empty!");
                    if (string.IsNullOrEmpty(APIKey))
                        throw new NullReferenceException("APIKey cannot be empty!");

                    NameValueCollection Paste = new NameValueCollection();
                    Paste.Add("api_dev_key", APIKey);
                    Paste.Add("api_option", "paste");
                    Paste.Add("api_paste_code", body);
                    Paste.Add("api_paste_private", "1");
                    Paste.Add("api_paste_name", "osu!Share");
                    Paste.Add("api_paste_format", "xml");
                    Paste.Add("api_paste_expire_date", "1H");

                    using (WebClient client = new WebClient())
                    {
                        string resp = Encoding.UTF8.GetString(client.UploadValues("http://pastebin.com/api/api_post.php", Paste));

                        Uri isValid = null;
                        if (!Uri.TryCreate(resp, UriKind.Absolute, out isValid))
                        {
                            throw new WebException("Paste error!", WebExceptionStatus.SendFailure);
                        }
                        else
                        {
                            Log.LogInfo(resp);
                            return resp;
                        }
                    }
                });
            return pushPaste.Result;
        }

        internal static string Get(string pasteID)
        {
            Task<string> getPaste = Task<string>.Factory.StartNew(() =>
                {
                    pasteID = pasteID.Replace("http://pastebin.com/", "");
                    pasteID = pasteID.Replace("pastebin.com/", "");
                    using (WebClient client = new WebClient())
                    {
                        string resp = null;
                        try { resp = client.DownloadString("http://pastebin.com/raw.php?i=" + pasteID); }
                        catch (Exception e)
                        {

                            Log.LogError("Pastebin.Get error!", e);
                        }
                        if (!string.IsNullOrEmpty(resp))
                            return resp;
                        else
                            throw new NullReferenceException("resp cannot be empty or null!");
                    }
                });
            return getPaste.Result;
        }
    }
}
