﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Drawing;

namespace osu_Share_ex
{
    internal class Lister2
    {
        static string AppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        static string FolderPath = Path.Combine(AppDataPath, "osu!Share");
        static string[] IgnoreStrings = { "<I N=\"", "\">", "<Root>", "</Root>", "<D>", "</D>", "<M>", "</M>", "</I>", "<?xml version=\"1.0\" encoding=\"utf-8\"?>", "<O>", "</O>", "<P>", "</P>", "<L>", "</L>", "<K>", "</K>", "<B>", "</B>", "<V>", "</V>", "<H>", "</H>" }; // P O L K B V
        static string[] IgnoreStringsWithoutSpace = { "(", ")", "[", "]" };

        private Dictionary<string, string> GetList(string p)
        {
            if (Directory.Exists(p))
            {
                Dictionary<string, string> Built = new Dictionary<string, string>();
                string[] Directories = Directory.GetDirectories(p);

                foreach (string sub in Directories)
                {
                    Built.Add(Path.GetFileName(sub), sub);
                }
                return Built;
            }
            else return null;
        }

        private List<BeatmapInfo2> CreateList(string path)
        {
            Dictionary<string, string> Data = GetList(path);
            List<BeatmapInfo2> MapInfo = new List<BeatmapInfo2>();

            if (Data != null)
            {
                foreach (var item in Data)
                {
                    try
                    {
                        string[] parts = item.Key.Split(new string[] { " " }, 2, StringSplitOptions.None);
                        BeatmapInfo2 b = new BeatmapInfo2();
                        b.ID = parts[0];
                        b.MapName = parts[1];
                        var ret = getOsuData(item.Value, parts[1]);
                        if (ret == null)
                            continue;
                        b.Difficulty = ret.Difficulties;
                        b.Modes = ret.Modes;

                        MapInfo.Add(b);
                    }
                    catch
                    {   }
                }
            }
            else return null;

            return MapInfo;
        }

        //  TODO:
        //   - Pastebin integration here
        internal string CreateXML(string path, bool local)
        {
            Task<string> woo = Task<string>.Factory.StartNew(() => {
                List<BeatmapInfo2> Data = CreateList(path);
                if (Data == null)
                    throw new NullReferenceException("Data cannot be empty!");

                XmlWriterSettings ws = new XmlWriterSettings();
                ws.Indent = true;

                XmlWriter w;
                if (local)
                    w = XmlWriter.Create(Path.Combine(FolderPath, "np.xml"));
                else
                    w = XmlWriter.Create("xml.np");

                w.WriteStartDocument();
                w.WriteStartElement("Root");

                foreach (BeatmapInfo2 b in Data) // P O L K B V
                {
                    w.WriteStartElement("I"); // I
                    w.WriteAttributeString("N", b.MapName); // N
                    w.WriteElementString("D", Main.Base64Encode(b.ID)); // D
                    //foreach (DifficultyInfo dInfo in b.Difficulty)
                    //{
                    //    w.WriteStartElement("H"); // H
                    //    w.WriteElementString("P", dInfo.DifficultyName); // Hn
                    //    w.WriteElementString("O", dInfo.Creator); // Hc
                    //    w.WriteElementString("L", dInfo.AR); // Ha
                    //    w.WriteElementString("K", dInfo.OD); // Ho
                    //    w.WriteElementString("B", dInfo.Drain); // Hd
                    //    w.WriteElementString("V", dInfo.CS); // Hs
                    //    w.WriteEndElement();
                    //}
                    w.WriteElementString("M", b.Modes); // M
                    w.WriteEndElement();
                }
                w.WriteEndElement();
                w.WriteEndDocument();
                w.Close();

                if (local)
                    return File.ReadAllText(Path.Combine(FolderPath, "np.xml"));
                else
                    return File.ReadAllText("np.xml");
            });
            return woo.Result;
        }

        private DifficultyReturn2 getOsuData(string path, string songname)
        {
            DifficultyReturn2 dReturn = new DifficultyReturn2();
            List<string> Modes = new List<string>();
            string ModesFinal;

            string[] Files = Directory.GetFiles(path);
            Dictionary<string, DifficultyInfo> Parsed = new Dictionary<string, DifficultyInfo>();
            List<DifficultyInfo> Final = new List<DifficultyInfo>();

            foreach (string s in Files)
            {
                if (s.Contains(".osu"))
                {
                    DifficultyInfo dInfo = new DifficultyInfo();

                    Dictionary<FileData, string> Data = ReadFile2(s);
                    if (!Data.ContainsKey(FileData.Mode))
                        Data.Add(FileData.Mode, "0");

                    if (!Modes.Contains(Data[FileData.Mode]))
                        Modes.Add(Data[FileData.Mode]);

                    var p = Path.GetFileNameWithoutExtension(s);
                    p = p.Replace(songname, "");

                    var sb = new StringBuilder();

                    // For getting creator
                    bool instr = false;
                    int CharInd1 = -1;
                    int CharInd2 = -1;

                    // For getting difficulty
                    bool instr2 = false;
                    int CharInt3 = -1;
                    int CharInt4 = -1;


                    string Creator = "N/A";
                    string Difficulty = "N/A";

                    for (int i = 0; i < p.Length; i++)
                    {
                        char c = p[i];
                        if (c == '(')
                        {
                            instr = true;
                            CharInd1 = i;
                        }
                        else if (c == ')')
                        {
                            CharInd2 = i;
                            if (instr)
                            {
                                if (CharInd1 != -1 && CharInd2 != -1)
                                {
                                    var length = CharInd2 - CharInd1;
                                    Creator = p.Substring(CharInd1 + 1, length - 1);
                                }
                            }
                        }
                        else if (c == '[')
                        {
                            instr2 = true;
                            CharInt3 = i;
                        }
                        else if (c == ']')
                        {
                            CharInt4 = i;
                            if (instr2)
                            {
                                if (CharInt3 != -1 && CharInt4 != -1)
                                {
                                    var length = CharInt4 - CharInt3;
                                    Difficulty = p.Substring(CharInt3 + 1, length - 1);
                                }
                            }
                        }
                    }
                    dInfo.Creator = Creator;
                    dInfo.DifficultyName = Difficulty;
                    try
                    {
                        dInfo.CS = Data[FileData.CS];
                    }
                    catch
                    {
                        dInfo.CS = "null";
                    }
                    try
                    {
                        dInfo.AR = Data[FileData.AR];
                    }
                    catch
                    {
                        dInfo.AR = "null";
                    }
                    try
                    {
                        dInfo.Drain = Data[FileData.DrainRate];
                    }
                    catch
                    {
                        dInfo.Drain = "null";
                    }
                    try
                    {
                        dInfo.OD = Data[FileData.OD];
                    }
                    catch
                    {
                        dInfo.OD = "null";
                    }

                    Final.Add(dInfo);
                }
            }

            if (Modes.Count > 0)
            {
                StringBuilder e = new StringBuilder();
                foreach (string m in Modes)
                {
                    e.Append(m + ",");
                }
                ModesFinal = e.ToString();
                ModesFinal = ModesFinal.Substring(0, ModesFinal.Length - 1);

                dReturn.Modes = ModesFinal;
                dReturn.Difficulties = Final;
                return dReturn;
            }
            else
                return null;
        }

        private Dictionary<FileData, string> ReadFile2(string path)
        {
            Dictionary<FileData, string> Built = new Dictionary<FileData,string>();

            string[] file = File.ReadAllLines(path);
            foreach(string line in file)
            {
                if (line.Contains("Mode:"))
                    Built.Add(FileData.Mode, line.Replace("Mode: ", ""));
                if (line.Contains("HPDrainRate:"))
                    Built.Add(FileData.DrainRate, line.Replace("HPDrainRate:", ""));
                if (line.Contains("CircleSize:"))
                    Built.Add(FileData.CS, line.Replace("CircleSize:", ""));
                if (line.Contains("OverallDifficulty:"))
                    Built.Add(FileData.OD, line.Replace("OverallDifficulty:", ""));
                if (line.Contains("ApproachRate:"))
                    Built.Add(FileData.AR, line.Replace("ApproachRate:", ""));
            }

            foreach (var item in Built)
                item.Value.Replace(" ", "");

            return Built;
        }

        [Obsolete ("Useless")]
        internal void Compress(string body)
        {
            Log.LogInfo("Starting compression");
            CompressProgressBar compressbar = new CompressProgressBar("Compress");
            string final = null;

            Thread thread = new Thread(() =>
            {
                string bodyCopy = body;
                string Final;
                int index = 0;
                List<string> AlreadyDone = new List<string>();

                foreach (string s in IgnoreStrings)
                {
                    body = body.Replace(s, " ");
                }
                foreach (string s in IgnoreStringsWithoutSpace)
                {
                    body = body.Replace(s, "");
                }
                string[] words = Regex.Split(body, " ");
                List<Tuple<string, int>> Complete = new List<Tuple<string, int>>();
                List<string> justString = new List<string>();

                Main.MainForm.Dispatcher.BeginInvoke((System.Action)delegate()
                {
                    compressbar.New(words.Length, "Parsing...");
                });
                Log.LogInfo("Starting list parsing");
                foreach (string s in words)
                {
                    if (AlreadyDone.Contains(s))
                        continue;
                    else
                        AlreadyDone.Add(s);

                    if (s.Length < 6)
                    {
                        Log.LogInfo("Skipped \"" + s + "\" because it was shorter than 6 letters!");
                        continue;
                    }

                    int matches = 0;
                    foreach (string x in words)
                    {
                        if (s == x)
                            matches++;
                        else
                            continue;
                    }

                    if (!justString.Contains(s))
                    {
                        justString.Add(s);
                        Complete.Add(new Tuple<string, int>(s, matches));
                        if (Main.Dev)
                            Log.LogInfo("Added \"" + s + "\" to compression list with " + matches + " matches");
                    }
                }

                Main.MainForm.Dispatcher.BeginInvoke((System.Action)delegate()
                {
                    compressbar.New(Complete.Count, "Compressing");
                });
                Log.LogInfo("Starting to compress");

                XmlWriter w;
                XmlWriterSettings ws = new XmlWriterSettings();
                ws.Indent = true;
                w = XmlWriter.Create(Path.Combine(FolderPath, "osu!Share_XML_temp.xml"), ws);
                w.WriteStartElement("Y");
                foreach (Tuple<string, int> s in Complete)
                {
                    bool test = CheckStringAmount(body, s.Item1, s.Item2);
                    if (test)
                    {
                        // <Y>
                        //  <E I="0000" T="" /> -> 7 + 21 + word_count
                        // </Y>
                        var Base64Ind = Main.Base64Encode(index.ToString());
                        w.WriteStartElement("E");
                        w.WriteAttributeString("I", Base64Ind);
                        w.WriteAttributeString("T", s.Item1);
                        w.WriteEndElement();

                        bodyCopy = bodyCopy.Replace(s.Item1, Base64Ind);
                        index++;

                        if (Main.Dev)
                            Log.LogInfo("Compressed \"" + s.Item1 + "\" with index " + Base64Ind);
                    }
                    else
                    {
                        continue;
                    }
                    Main.MainForm.Dispatcher.BeginInvoke((System.Action)delegate()
                    {
                        compressbar.Update();
                    });
                }
                w.WriteEndElement();
                w.Flush();
                Final = bodyCopy;
                Log.LogInfo("Compression finished");

                Main.MainForm.Dispatcher.BeginInvoke((System.Action)delegate()
                {
                    compressbar.Close();
                });
                if (index > 0)
                    final = Final;
                else
                {
                    MessageBox.Show("foo");
                    final = null;
                }
            });
            thread.Start();
            thread.Join();

            if (final != null)
                File.WriteAllText(System.IO.Path.Combine(FolderPath, "np2.xml"), final);
        }

        private bool CheckStringAmount(string body, string compareTo, int matches)
        {
            // 'in in' - 5 letters vs 
            // '<I ind="0">in</I>' and '{0}' - 22 letters

            try
            {
                int matches1 = Regex.Matches(body, compareTo).Count;
                int compareToLetters = compareTo.Length;
                int totalLetterAmount = matches1 * compareToLetters;

                if (compareToLetters < 4)
                    return false;

                if (totalLetterAmount > ((compareToLetters * matches) + 28))
                    return true;
                else
                    return false;
            }
            catch { return false; }
        }
    }

     internal class CompressProgressBar : IDisposable
    {
        private Form mainform;
        private ProgressBar progressBar;
        private Label progressText;

        private int progressMax;
        private int currentProgress = 0;
        
        public void Dispose()
        {
            Dispose();
            mainform.Dispose();
            progressBar.Dispose();
            progressText.Dispose();
            GC.SuppressFinalize(this);
        }

        internal CompressProgressBar(string formName, int max = 100)
        {
            this.progressMax = max;

            this.mainform = new Form();
            this.progressBar = new ProgressBar();
            this.progressText = new Label();

            this.mainform.Text = formName;
            this.mainform.StartPosition = FormStartPosition.CenterParent;
            this.mainform.Size = new Size(200, 100);

            this.progressBar.Maximum = max;
            this.progressBar.Location = new Point(5, 5);
            this.progressBar.Size = new Size(190, 20);
            this.progressBar.Style = ProgressBarStyle.Continuous;

            this.progressText.Location = new Point(5, 30);
            this.progressText.Text = currentProgress.ToString() + "/" + max.ToString();

            this.mainform.Controls.Add(progressBar);
            this.mainform.Controls.Add(progressText);

            this.mainform.Show();
        }

        internal void Update()
        {
            this.currentProgress++;
            this.progressText.Text = currentProgress.ToString() + "/" + progressMax.ToString();
            this.progressBar.Value = currentProgress;
        }

        internal void Update(int amount)
        {
            this.currentProgress = amount;
            this.progressText.Text = currentProgress.ToString() + "/" + progressMax.ToString();
            this.progressBar.Value = currentProgress;
        }

        internal void Close()
        {
            this.mainform.Close();
        }

        internal void Reset()
        {
            this.currentProgress = 0;
            this.progressText.Text = currentProgress.ToString() + "/" + progressMax.ToString();
        }

        internal void SetMax(int newMax)
        {
            this.Reset();
            this.progressBar.Maximum = newMax;
        }

        internal void New(int newMax, string formName)
        {
            this.SetMax(newMax);
            this.mainform.Text = formName;
        }

        
    }

    internal class Read2
    {
        internal List<BeatmapInfo2> GetList(string path = "np.xml")
        {
            List<BeatmapInfo2> Data = new List<BeatmapInfo2>();
            XmlDocument doc = new XmlDocument();

            if (path.Contains("pastebin"))
            {
                var resp = Pastebin.Get(path);
                doc.LoadXml(resp);

                var node = doc.GetElementsByTagName("I");
                Log.LogInfo("Item count: " + node.Count.ToString());
                foreach (XmlNode n in node)
                {
                    var r = Parse(n);
                    Data.Add(r);
                }
                return Data;
            }
            else
            {
                try
                {
                    doc.Load(path);
                    var node = doc.GetElementsByTagName("I");
                    Log.LogInfo("Item count: " + node.Count.ToString());
                    foreach (XmlNode n in node)
                    {
                        var r = Parse(n);
                        Data.Add(r);
                    }

                    return Data;
                }
                catch
                {
                    return null;
                }
            }
        }

        internal BeatmapInfo2 Parse(XmlNode node)
        {
            BeatmapInfo2 b = new BeatmapInfo2();
            List<DifficultyInfo> Difficulty = new List<DifficultyInfo>();

            b.MapName = node.Attributes["N"].Value;
            foreach (XmlNode n in node)
            {
                if (n.Name == "D")
                {
                    b.ID = Main.Base64Decode(n.InnerText);
                    b.ID = b.ID.Replace(" ", "");
                }
                else if (n.Name == "M")
                    b.Modes = n.InnerText;
                else if (n.Name == "H")
                {
                    XmlNodeList nList = n.ChildNodes;
                    DifficultyInfo dInfo = new DifficultyInfo();
                    foreach (XmlNode n2 in nList)
                    {
                        if (n2.Name == "P")
                            dInfo.DifficultyName = n2.InnerText;
                        else if (n2.Name == "O")
                            dInfo.Creator = n2.InnerText;
                        else if (n2.Name == "L")
                            dInfo.AR = n2.InnerText;
                        else if (n2.Name == "K")
                            dInfo.OD = n2.InnerText;
                        else if (n2.Name == "B")
                            dInfo.Drain = n2.InnerText;
                        else if (n2.Name == "V")
                            dInfo.CS = n2.InnerText;
                    }
                    b.Difficulty.Add(dInfo);
                }
            }
            return b;
        }

        internal List<BeatmapInfo2> Compare(string mainpath = "np.xml", string secondarypath = "np.xml")
        {
            List<BeatmapInfo2> Data1 = GetList(mainpath);
            List<BeatmapInfo2> Data2 = GetList(secondarypath);

            List<BeatmapInfo2> ReturnData = new List<BeatmapInfo2>();

            foreach (BeatmapInfo2 b in Data1)
            {
                var bName = b.MapName;
                var bId = b.ID;
                var Add = true;
                foreach (BeatmapInfo2 b2 in Data2)
                {
                    if (b2.MapName == bName && b2.ID == bId)
                    {
                        Add = false;
                        break;
                    }
                }
                if (Add)
                    ReturnData.Add(b);
            }

            return ReturnData;
        }
    }

    public class BeatmapInfo2
    {
        public string ID { get; set; }
        public string MapName { get; set; }

        public List<DifficultyInfo> Difficulty = new List<DifficultyInfo>();
        public string Modes { get; set; }
    }

    public class DifficultyInfo
    {
        public string Creator;
        public string DifficultyName;

        public string AR;
        public string Drain;
        public string CS;
        public string OD;
    }

    public class DifficultyReturn2
    {
        public List<DifficultyInfo> Difficulties = new List<DifficultyInfo>();
        public string Modes;
    }

    public enum FileData
    {
        Mode,
        DrainRate,
        CS,
        OD,
        AR
    }
}
