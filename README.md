**Any bugs found should still be reported on the issue tracker! Also if you have any features in mind you can create a new issue and select enhancement!**

**Please read the README.txt file if you decide to download**

![osu!shareBanner.png](https://bitbucket.org/repo/9yqABE/images/388222839-osu!shareBanner.png)
## What is osu!Share? ##
osu!Share is a program that you can use to share [osu!](https://osu.ppu.sh) beatmaps with your friends or use it to backup your beatmaps.

osu!Share is currently in active development and is in **BETA**, this means that the program will not be downloadable yet, but the source is open. Once I get a usable version of the program done this page will have pictures added and a downloadable executable will be added to downloads.

## Features ##
* Reliable
* Fast (Depending on internet/computer speed)
* Easy to use
* Lightweight

## Current requirements ##
**These requirements may change during development**

* Windows 7+
* .Net Framework 4.5
* Internet connection (Faster is better)

## Images ##
![osu!Share_2015-04-30_15-46-05.png](https://bitbucket.org/repo/9yqABE/images/732116336-osu!Share_2015-04-30_15-46-05.png)
![osu!Share_2015-04-30_15-41-45.png](https://bitbucket.org/repo/9yqABE/images/1464916278-osu!Share_2015-04-30_15-41-45.png)
![osu!Share_2015-04-30_15-41-48.png](https://bitbucket.org/repo/9yqABE/images/3063678702-osu!Share_2015-04-30_15-41-48.png)
![osu!Share_2015-04-30_15-39-09.png](https://bitbucket.org/repo/9yqABE/images/605917935-osu!Share_2015-04-30_15-39-09.png)

## ROADMAP ##
1. Alpha - All technical stuff, ironing out bugs etc. (**DONE**)
2. Beta - Making user interface beautiful, opening the source.
3. Pre-release - Fixing all bugs.
4. Release - Woooo.

**Currently it would look like osu!Share will be in development for 6-12 months**

## Reporting bugs ##
This program is in beta so bugs are bound to happen. I do try my best to fix as many bugs I can find before shipping a product, but I'm just one guy. So if you happen to find any bugs, please report them to the [issue tracker](https://bitbucket.org/DoubleKilled/osushare-beta/issues).